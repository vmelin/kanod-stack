#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

METAL3_RELEASE=v1.1.2
CAPI_RELEASE=v1.1.3
CLUSTERDEF_RELEASE=v0.7.9
IRONIC_CREDENTIAL_RELEASE=0.2.0
CERT_MANAGER_RELEASE=v1.5.3
NGINX_INGRESS_RELEASE=v1.2.1
METALLB_RELEASE=v0.12.1
REGISTRAR_RELEASE=v0.2.1

sync: sync-ironic sync-capi sync-capm3 sync-argocd sync-cert sync-build sync-job sync-ingress sync-registrar

sync-ironic:
	git clone https://github.com/metal3-io/baremetal-operator.git
	cd baremetal-operator && git checkout capm3-$(METAL3_RELEASE)
	# update ironic
	rm -rf ironic/ironic-deployment
	rm -rf bmoperator/config
	cp -r baremetal-operator/ironic-deployment ironic/
	cp -r baremetal-operator/config bmoperator
	sed 's/DEPLOY_KERNEL_URL=.*$/DEPLOY_KERNEL_URL=\${IPA_KERNEL_URL}/g' bmoperator/ironic.env
	sed 's/DEPLOY_RAMDISK_URL=.*$/DEPLOY_RAMDISK_URL=\${IPA_RAMDISK_URL}/g' bmoperator/ironic.env

	mkdir -p ironic/ironic-deployment/config/namespace
	touch ironic/ironic-deployment/config/namespace/kustomization.yaml
	yq e 'del(.secretGenerator)' -i bmoperator/config/basic-auth/tls/kustomization.yaml
	yq e 'del(.secretGenerator)' -i ironic/ironic-deployment/default/kustomization.yaml
	rm -rf baremetal-operator

sync-capi:
	curl -L -o capi/core/bootstrap.yaml https://github.com/kubernetes-sigs/cluster-api/releases/download/${CAPI_RELEASE}/bootstrap-components.yaml
	curl -L -o capi/core/controlplane.yaml https://github.com/kubernetes-sigs/cluster-api/releases/download/${CAPI_RELEASE}/control-plane-components.yaml
	curl -L -o capi/core/core.yaml https://github.com/kubernetes-sigs/cluster-api/releases/download/${CAPI_RELEASE}/core-components.yaml
	sed -i 's/\$${[A-Z_]*:=false}/false/g' capi/core/bootstrap.yaml
	sed -i 's/\$${EXP_CLUSTER_RESOURCE_SET:=false}/true/g' capi/core/core.yaml
	sed -i 's/\$${[A-Z_]*:=false}/false/g' capi/core/core.yaml

sync-cert:
	curl -L -o capi/cert-manager.yaml https://github.com/jetstack/cert-manager/releases/download/${CERT_MANAGER_RELEASE}/cert-manager.yaml

sync-capm3:
	curl -L -o capi/capm3/capm3.yaml https://github.com/metal3-io/cluster-api-provider-metal3/releases/download/$(METAL3_RELEASE)/infrastructure-components.yaml

sync-argocd:
	curl -L --output argocd/install.yaml https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

sync-clusterdef:
	git clone `git remote get-url origin | sed 's/kanod-stack/cluster-def/'` cdef
	cd cdef && git checkout $(CLUSTERDEF_RELEASE) && MANIFEST_ONLY=1 NEXUS_REGISTRY='$${NEXUS_REGISTRY}' REPO_URL=y IMAGE_SYNC_DIR=z VERSION=$(CLUSTERDEF_RELEASE) ./build.sh && cp manifest.yml ../clusterdef/clusterdef.yaml
	rm -rf cdef

sync-registrar:
	git clone `git remote get-url origin | sed 's/kanod-stack/tpm-registrar/'` reg
	cd reg && git checkout $(REGISTRAR_RELEASE) &&  REGISTRAR_VERSION="${REGISTRAR_RELEASE}" envsubst '$${REGISTRAR_VERSION}' < manifest.yaml > ../registrar/registrar.yaml
	rm -rf reg


sync-build:
	sed -i "s/^METAL3_RELEASE=.*$$/METAL3_RELEASE=$(METAL3_RELEASE)/" ../ci-build.sh
	sed -i "s/^METAL3_RELEASE=.*$$/METAL3_RELEASE=$(METAL3_RELEASE)/" ../ironic-image-container.sh

sync-job:
	git clone https://gitlab.com/Orange-OpenSource/kanod/ironic-credentials.git ironic-creds
	cd ironic-creds && git checkout $(IRONIC_CREDENTIAL_RELEASE) && cp manifest.yaml ../ironic/kanod/ironic-credentials.yaml
	sed -i "s/^IRONIC_CREDENTIAL_RELEASE=.*$$/IRONIC_CREDENTIAL_RELEASE=$(IRONIC_CREDENTIAL_RELEASE)/" ../ci-build.sh
	rm -rf ironic-creds

sync-ingress:
	curl -L https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-$(NGINX_INGRESS_RELEASE)/deploy/static/provider/cloud/deploy.yaml | sed 's/@sha256:[0-9a-z]*$$//' | grep -v '^#GENERATED FOR' > ingress/nginx/nginx.yaml
	kustomize build https://github.com/metallb/metallb/manifests?ref=$(METALLB_RELEASE) > ingress/metallb/metallb.yaml
