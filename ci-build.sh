#!/bin/bash

set -eu
set -o pipefail

METAL3_RELEASE=v1.1.2
IRONIC_CREDENTIAL_RELEASE=0.2.0
KUSTOMIZE_VERSION=${KUSTOMIZE_VERSION:-4.5.4}

export METAL3_RELEASE
export IRONIC_CREDENTIAL_RELEASE
export KANOD_STACK_VERSION="${STACK_VERSION%.*([0-9])}"

basedir=$(dirname "${BASH_SOURCE[0]}")
builddir="${basedir}/build"
mkdir -p "${builddir}"

target="${builddir}/stack-raw.yml"


if [ ! -f kustomize ]; then
   curl -L -o kustomize.tgz https://github.com/kubernetes-sigs/kustomize/releases/"download/kustomize%2Fv${KUSTOMIZE_VERSION}/kustomize_v${KUSTOMIZE_VERSION}_linux_amd64.tar.gz"
   tar -zxvf kustomize.tgz kustomize
   rm kustomize.tgz
fi

# shellcheck disable=SC2016
./kustomize build "${basedir}"/manifests | envsubst '$KANOD_STACK_VERSION,$METAL3_RELEASE,$IRONIC_CREDENTIAL_RELEASE' > "${target}"

# We do not enable ClusterClass as we have our own abstractions
# shellcheck disable=SC2016
sed -i 's/\${CLUSTER_TOPOLOGY:=false}/false/' "${target}"
# We do not use ignition for bootstrap
# shellcheck disable=SC2016
sed -i 's/\${EXP_KUBEADM_BOOTSTRAP_FORMAT_IGNITION:=false}/false/' "${target}"
# We set MARIADB_HOST_IP to localhost (it is not a variable !)
sed -i 's/MARIADB_HOST_IP/127.0.0.1/' "${target}"
# We set IRONIC_HOST_IP to variable IRONIC_IP
# All the ${pass:-""} keep ${pass}
sed -i 's/\${\([A-Z0-9_]*\):-""}/${\1}/' "${target}"
# All the ${flag:='false'} to false
sed -i 's/\${\([A-Z0-9_]*\):='"'"'false'"'"'}/'"'"'false'"'"'/' "${target}"
# All the IRONIC_NO_XXX:-path, keep path
sed -i 's/\${[A-Z0-9_]*:-\([^}]*\)}/\1/' "${target}"
# Quote variables occuring alone
sed -i "s/: \\(\\\${.*}\\)\$/: '\\1'/" "${target}"

for entry in ironic bmoperator capi argocd clusterdef dashboard; do
   # shellcheck disable=SC2016
   ./kustomize build "${basedir}/manifests/${entry}" | envsubst '$KANOD_STACK_VERSION,$METAL3_RELEASE,$IRONIC_CREDENTIAL_RELEASE' > "${basedir}/build/kr-${entry}.yml"
done

sed -i 's/MARIADB_HOST_IP/127.0.0.1/' "${basedir}/build/kr-ironic.yml"

# shellcheck disable=SC2016
./kustomize build "${basedir}"/kanod-operator/config/kanod | envsubst '$KANOD_STACK_VERSION' > build/kanod-operator.yml

# create manifests for ingress
./kustomize build "${basedir}"/manifests/ingress/floating > build/kanod-ingress-raw.yml

# create manifests for registrar
./kustomize build "${basedir}"/manifests/registrar > build/tpm-registrar.yml
