#!/bin/bash

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -eu

basedir=$(readlink -f "$(dirname "${BASH_SOURCE[0]}")")

KUSTOMIZE_VERSION=${KUSTOMIZE_VERSION:-4.5.4}

echo "Sync images for cert manager"
# shellcheck disable=SC2016
kanod-image-sync --parse-stdin --user "$NEXUS_KANOD_USER" --password "$NEXUS_KANOD_PASSWORD" --tag "${TAG}" --registry "${NEXUS_REGISTRY}" --export --exported-registry '${NEXUS_REGISTRY}' < manifests/capi/cert-manager.yaml > build/image-kustomization-cm.yml
echo
echo "Sync images for stack"
# shellcheck disable=SC2016
kanod-image-sync --parse-stdin --user "$NEXUS_KANOD_USER" --password "$NEXUS_KANOD_PASSWORD" --tag "${TAG}" --registry "${NEXUS_REGISTRY}" --export --exported-registry '${NEXUS_REGISTRY}' < build/stack-raw.yml > build/image-kustomization.yml
echo
echo "Sync images for ingress"
# shellcheck disable=SC2016
kanod-image-sync --parse-stdin --user "$NEXUS_KANOD_USER" --password "$NEXUS_KANOD_PASSWORD" --tag "${TAG}" --registry "${NEXUS_REGISTRY}" --export --exported-registry '${NEXUS_REGISTRY}' < build/kanod-ingress-raw.yml > build/image-kustomization-ing.yml
echo
echo "Sync components"
for entry in ironic bmoperator capi argocd clusterdef dashboard; do
  echo "- ${entry}"
  # shellcheck disable=SC2016
  kanod-image-sync \
    --parse-stdin --user "$NEXUS_KANOD_USER" --password "$NEXUS_KANOD_PASSWORD" \
    --tag "${TAG}" --registry "${NEXUS_REGISTRY}" --export \
    --exported-registry '${NEXUS_REGISTRY}' < "build/kr-${entry}.yml" > "build/ik-${entry}.yml"
done
echo

echo "Renaming stack"

relocate() {
  echo "- Relocate $3"
  renaming=$(mktemp -d)
  cp "build/$2" "${renaming}/kustomization.yaml"
  cp "$1" "${renaming}/manifest.yaml"
  (cd "$renaming" && "${basedir}/kustomize" edit add resource manifest.yaml)
  # shellcheck disable=SC2016
  ./kustomize build "${renaming}" | sed "s/: \\(\\\${[^'}]*}\\)\$/: '\\1'/" > "build/$3"
  rm -rf "${renaming}"
}

if [ ! -f kustomize ]; then
  curl -L -o kustomize.tgz "https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2Fv${KUSTOMIZE_VERSION}/kustomize_v${KUSTOMIZE_VERSION}_linux_amd64.tar.gz"
  tar -zxvf kustomize.tgz kustomize
  rm kustomize.tgz
fi

relocate build/stack-raw.yml image-kustomization.yml stack.yml
relocate "${basedir}/manifests/capi/cert-manager.yaml" image-kustomization-cm.yml cert-manager.yml
relocate build/kanod-ingress-raw.yml image-kustomization-ing.yml kanod-ingress.yml
for entry in ironic bmoperator capi argocd clusterdef dashboard; do
  relocate "build/kr-${entry}.yml" "ik-${entry}.yml" "kn-${entry}.yml"
done

# Adds applications to result
cp "${basedir}/manifests/argocd/applications.yaml" "${basedir}/build/applications.yml"
