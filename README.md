# KANOD stack

Common stack for the management cluster. The stack should be specialized with `envsubst`. The variables
are the following:

* `MGT_HTTP_PROXY` HTTP_PROXY for the management cluster
* `MGT_HTTPS_PROXY` HTTPS_PROXY for the management cluster
* `MGT_NO_PROXY` NO_PROXY for the management cluster
* `IRONIC_IP` the IP address of the Ironic instance on the PXE network.
* `DHCP_RANGE` the range of address used for the DHCP server.
* `DHCP_HOSTS` this variable may be set or not:
    * if it is not set, any DHCP (PXE, iPXE) request will be honored.
    * if it is set. Ironic will only respond to hosts described in the list that
      should follow the syntax `name1,mac1 name2,mac2 ... name_n,mac_n` where `name`
      is the common name of a machine and `mac` is the mac address (only the mac is
      really relevant). Elements of a pair must be separated by a single comma and
      pairs must be separated by a space.
* `IPA_URL` URL (usually on the nexus) of a tar file containing the kernel and the ramdisk of the Ironic Python Agent
* `PXE_ITF`interface on which ironic listens for PXE request
* `NEXUS_REGISTRY` address of the docker registry hosting the image specific to kanod. It is usually
   the nexus that is used as a private registry hence the variable name.
* `BAREMETAL_GIT` HTTP Url for the git containing host definitions
* `BASE64_BAREMETAL_USERNAME` username to access this git in base64 format
* `BASE64_BAREMETAL_PASSWORD` password to access this git in base64 format
* `PROJECTS_GIT` HTTP Url for the git containing projects definitions
* `BASE64_PROJECTS_USERNAME` username to access this git in base64 format
* `BASE64_PROJECTS_PASSWORD` password to access this git in base64 format
* `KANOD_CLUSTER_VERSION` the version of the cluster templates on the Nexus.
  (eg. `v0.0.4`)

## Usage from the command-line (eg. with minikube ci)
* `config` must be updated before deployment.
* `kanod-stack.sh` is the entry point. It assumes a correct kubeconfig

## Generation for the ci
use ci-script to generate and upload a non specialized version of the stack on Nexus.

* `REPO_URL` the url of the Nexus.
* `STACK_VERSION` the semantic version identifier of the stack (in the format `major.minor.patch`)

On the ci we use the current tag for `STACK_VERSION`
