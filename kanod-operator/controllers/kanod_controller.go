/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"reflect"
	"strconv"
	"time"

	"github.com/go-logr/logr"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/util/retry"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"

	configv1 "gitlab.com/Orange-OpenSource/kanod/kanod-operator/api/v1"
	"gitlab.com/Orange-OpenSource/kanod/kanod-operator/resources"
)

const (
	RETRY_DELAY  = 5
	IPA_ARTIFACT = "ironic-python-agent"
)

type State = int

const (
	Initial State = iota
	CertManagerDeploy
	CertManagerWaitEndpoint
	CertManagerWaitStable
	IronicDeploy
	DhcpDeploy
	BmoDeploy
	CapiDeploy
	ArgoCDDeploy
	ClusterDefDeploy
	DashboardDeploy
	IngressDeploy
	TpmRegistrarDeploy
	GitCertsDeploy
	StackWaitStable
	ApplicationDeploy
	ApplicationWaitStable
	Final
)

// KanodReconciler reconciles a Kanod object
type KanodReconciler struct {
	client.Client
	Log        logr.Logger
	RestConfig *rest.Config
	Config     *KanodConfig
	RepoClient *http.Client
}

// KanodConfig is the static part of the operator configuration retrieved
// mainly from the configMap in kanod.
type KanodConfig struct {
	RepoUrl    string
	RepoCa     string
	Registry   string
	HttpProxy  string
	HttpsProxy string
	NoProxy    string
	VaultUrl   string
	VaultCa    string
	VaultRole  string
}

func (r *KanodReconciler) NewKanodConfig(
	ctx context.Context,
	name string,
	namespace string,
) (*KanodConfig, error) {
	var config = &corev1.ConfigMap{}
	cmapMeta := client.ObjectKey{
		Name:      name,
		Namespace: namespace,
	}
	err := r.Client.Get(ctx, cmapMeta, config)
	if err != nil {
		r.Log.Error(err, "Cannot find the base config", "configName", name)
		return nil, err
	}
	repository, ok := config.Data["repository"]
	if !ok {
		r.Log.Error(err, "Cannot proceed without repo", "configName", name)
		return nil, err
	}
	registry, ok := config.Data["registry"]
	if !ok {
		r.Log.Error(err, "Cannot proceed without registry", "configName", name)
		return nil, err
	}
	var kanodConfig = KanodConfig{
		RepoUrl:    repository,
		RepoCa:     config.Data["repo_ca"],
		Registry:   registry,
		HttpProxy:  config.Data["http_proxy"],
		HttpsProxy: config.Data["https_proxy"],
		NoProxy:    config.Data["no_proxy"],
		VaultUrl:   config.Data["vault_url"],
		VaultCa:    config.Data["vault_ca"],
		VaultRole:  config.Data["vault_role"],
	}
	return &kanodConfig, nil
}

func (r *KanodReconciler) NewHttpClient() *http.Client {
	pool := x509.NewCertPool()
	if r.Config.RepoCa != "" {
		pool.AppendCertsFromPEM([]byte(r.Config.RepoCa))
	}
	tlsConfig := &tls.Config{
		RootCAs: pool,
	}
	transport := &http.Transport{TLSClientConfig: tlsConfig}
	return &http.Client{Transport: transport}
}

func b64(s string) string {
	return base64.StdEncoding.EncodeToString([]byte(s))
}

type Env struct {
	Env    map[string]string
	Errors []string
}

func (e *Env) Getenv(key string) string {
	v, ok := e.Env[key]
	if !ok {
		e.Errors = append(e.Errors, key)
	}
	return v
}

func (e *Env) Setenv(key string, val string) {
	e.Env[key] = val
}

//+kubebuilder:rbac:groups=config.kanod.io,resources=kanods,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=config.kanod.io,resources=kanods/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=config.kanod.io,resources=kanods/finalizers,verbs=update
//+kubebuilder:rbac:groups="",resources="configmaps",verbs=get;list;create;update;watch
func (r *KanodReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {

	var kanod configv1.Kanod
	if err := r.Get(ctx, req.NamespacedName, &kanod); err != nil {
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}
	log := r.Log.WithValues("name", kanod.Name, "namespace", kanod.Namespace)
	if r.Config == nil {
		var err error
		r.Config, err = r.NewKanodConfig(
			ctx, kanod.Spec.ConfigName, kanod.Namespace)
		if err != nil {
			return ctrl.Result{}, err
		}
	}
	var env = &Env{Env: make(map[string]string)}
	env.Setenv("NEXUS_REGISTRY", r.Config.Registry)
	env.Setenv("NEXUS", r.Config.RepoUrl)
	if r.Config.RepoCa != "" {
		env.Setenv("BASE64_REPO_CA", base64.StdEncoding.EncodeToString([]byte(r.Config.RepoCa)))
	}
	env.Setenv("MGT_HTTP_PROXY", r.Config.HttpProxy)
	env.Setenv("MGT_HTTPS_PROXY", r.Config.HttpsProxy)
	env.Setenv("MGT_NO_PROXY", r.Config.NoProxy)
	env.Setenv("VAULT_URL", r.Config.VaultUrl)
	env.Setenv("VAULT_CA_B64", b64(r.Config.VaultCa))
	env.Setenv("VAULT_K8S", r.Config.VaultRole)

	// TODO: support for TPM.

	ironic := kanod.Spec.Ironic
	if ironic != nil {
		env.Setenv("IRONIC_IP", ironic.IP)
		if ironic.IpaKernel != "" {
			env.Setenv("IPA_KERNEL_URL", ironic.IpaKernel)
			env.Setenv("IPA_RAMDISK_URL", ironic.IpaRamdisk)
		} else {
			release, err := r.MavenRelease(IPA_ARTIFACT)
			if err != nil {
				log.Error(err, "cannot get IPA Url")
				return ctrl.Result{}, err
			}
			env.Setenv("IPA_KERNEL_URL", r.ArtifactUrl(IPA_ARTIFACT, release, "kernel", "kernel"))
			env.Setenv("IPA_RAMDISK_URL", r.ArtifactUrl(IPA_ARTIFACT, release, "initramfs", "initramfs"))
		}
		env.Setenv("PXE_ITF", ironic.Interface)
		if ironic.Tpm != nil {
			authCa, err := r.GetValue(ctx, ironic.Tpm.AuthCa, kanod.Namespace)
			if err != nil {
				log.Error(err, "cannot get TPM CA")
				return ctrl.Result{}, err
			}
			env.Setenv("TPM_AUTH_CA_B64", b64(authCa))
			env.Setenv("TPM_AUTH_URL", ironic.Tpm.AuthUrl)
		} else {
			env.Setenv("TPM_AUTH_CA_B64", "")
			env.Setenv("TPM_AUTH_URL", "")
		}
	}

	ingress := kanod.Spec.Ingress
	if ingress != nil {
		env.Setenv("INGRESS_IP", ingress.IP)
		env.Setenv("INGRESS_NAME", ingress.Name)
		if ingress.Key != nil {
			value, _ := r.GetValue(ctx, ingress.Key, kanod.Namespace)
			env.Setenv("INGRESS_KEY_B64", b64(value))
			value, _ = r.GetValue(ctx, ingress.Certificate, kanod.Namespace)
			env.Setenv("INGRESS_CERT_B64", b64(value))
			env.Setenv("INGRESS_SECRET", "ingress-external-cert")
		} else {
			env.Setenv("INGRESS_KEY_B64", "")
			env.Setenv("INGRESS_CERT_B64", "")
			env.Setenv("INGRESS_SECRET", "ingress-cert")
		}
		env.Setenv("LB_RANGE", ingress.LBRange)
	}

	argocd := kanod.Spec.ArgoCD
	if argocd != nil {
		r.configureProject(ctx, env, &kanod, "BAREMETAL", &argocd.Baremetal)
		r.configureProject(ctx, env, &kanod, "PROJECTS", &argocd.Projects)
	}

	step, recDate, err := r.getProgression(&kanod)
	if err != nil {
		r.setProgression(ctx, &kanod, 0)
		return ctrl.Result{}, err
	}
	switch step {
	case Initial:
		if !r.checkPodsReady(ctx, []string{"kube-system"}) {
			return ctrl.Result{RequeueAfter: time.Second * RETRY_DELAY}, nil
		}
		if err := r.setProgression(ctx, &kanod, CertManagerDeploy); err != nil {
			log.Error(err, "Cannot set annotation for cert-manager apply")
			return ctrl.Result{}, err
		}
		return ctrl.Result{RequeueAfter: time.Second * RETRY_DELAY}, nil
	case CertManagerDeploy:
		return r.applyComponent(
			kanod.Spec.CertManager,
			ctx, env, &kanod, "cert-manager",
			resources.CertManager,
			configv1.CertManagerReadyCondition,
			CertManagerWaitEndpoint)
	case CertManagerWaitEndpoint:
		if r.checkCertmanagerEndpoints(ctx) {
			log.Info("Cert manager is ready")
			meta.SetStatusCondition(
				&kanod.Status.Conditions,
				metav1.Condition{
					Type:   configv1.CertManagerReadyCondition,
					Status: metav1.ConditionTrue,
					Reason: configv1.ComponentDeployedReason,
				})
			if err := r.updateKanodStatus(&kanod); err != nil {
				return ctrl.Result{}, err
			}
			if err := r.setProgression(ctx, &kanod, CertManagerWaitStable); err != nil {
				log.Error(err, "Cannot set annotation for cert-manager wait")
				return ctrl.Result{}, err
			}
		}
		return ctrl.Result{RequeueAfter: time.Second * RETRY_DELAY}, nil
	case CertManagerWaitStable:
		if recDate+RETRY_DELAY*4 < time.Now().Unix() {
			log.Info("Additional delay cert manager")
			return ctrl.Result{RequeueAfter: time.Second * RETRY_DELAY}, nil
		}
		if err := r.setProgression(ctx, &kanod, IronicDeploy); err != nil {
			log.Error(err, "Cannot set annotation for requeue delay")
			return ctrl.Result{}, err
		}
		return ctrl.Result{RequeueAfter: time.Second * RETRY_DELAY}, nil
	case IronicDeploy:
		meta.SetStatusCondition(
			&kanod.Status.Conditions,
			metav1.Condition{
				Type:   configv1.KanodStackReadyCondition,
				Status: metav1.ConditionFalse,
				Reason: configv1.WaitingForComponentReason,
			})
		if err := r.updateKanodStatus(&kanod); err != nil {
			return ctrl.Result{}, err
		}
		return r.applyComponent(
			kanod.Spec.Ironic != nil,
			ctx, env, &kanod, "ironic", resources.Ironic, configv1.BaremetalReadyCondition, DhcpDeploy)
	case DhcpDeploy:
		if ironic != nil && ironic.DHCP != nil {
			if err := r.configureDhcp(ctx, log, ironic.DHCP); err != nil {
				return ctrl.Result{}, err
			}
		}
		if err := r.setProgression(ctx, &kanod, BmoDeploy); err != nil {
			return ctrl.Result{}, err
		}
		return ctrl.Result{RequeueAfter: time.Second * RETRY_DELAY}, nil
	case BmoDeploy:
		return r.applyComponent(
			kanod.Spec.Ironic != nil && kanod.Spec.Ironic.Bmo,
			ctx, env, &kanod, "baremetal-operator", resources.BMO, "", CapiDeploy)
	case CapiDeploy:
		return r.applyComponent(
			kanod.Spec.Capi,
			ctx, env, &kanod, "capi", resources.Capi, configv1.ClusterApiReadyCondition, ArgoCDDeploy)
	case ArgoCDDeploy:
		return r.applyComponent(
			kanod.Spec.ArgoCD != nil,
			ctx, env, &kanod, "argocd", resources.Argocd, configv1.GitopsReadyCondition, ClusterDefDeploy)
	case ClusterDefDeploy:
		return r.applyComponent(
			kanod.Spec.ArgoCD != nil && kanod.Spec.ArgoCD.ClusterDef,
			ctx, env, &kanod, "clusterdef", resources.Clusterdef, configv1.ClusterdefReadyCondition, DashboardDeploy)
	case DashboardDeploy:
		return r.applyComponent(
			kanod.Spec.ArgoCD != nil && kanod.Spec.ArgoCD.Dashboard,
			ctx, env, &kanod, "dashboard", resources.Dashboard, "", IngressDeploy)
	case IngressDeploy:
		return r.applyComponent(
			kanod.Spec.Ingress != nil,
			ctx, env, &kanod, "ingress", resources.Ingress, configv1.IngressReadyCondition, TpmRegistrarDeploy)
	case TpmRegistrarDeploy:
		if kanod.Spec.Ironic != nil && kanod.Spec.Ironic.Tpm != nil {
			registrar := kanod.Spec.Ironic.Tpm.Registrar
			if registrar != nil {
				env.Setenv("REGISTRAR_IP", registrar.IP)
				env.Setenv("REGISTRAR_PORT", strconv.Itoa(registrar.Port))
			} else if kanod.Spec.Ingress != nil {
				env.Setenv("REGISTRAR_IP", kanod.Spec.Ingress.IP)
				env.Setenv("REGISTRAR_PORT", "443")
			} else {
				env.Setenv("REGISTRAR_IP", kanod.Spec.Ironic.IP)
				env.Setenv("REGISTRAR_PORT", "8443")
			}
			env.Setenv("REGISTRAR_CA_B64", b64(r.Config.VaultCa))
		}
		return r.applyComponent(
			kanod.Spec.Ironic != nil && kanod.Spec.Ironic.Tpm != nil,
			ctx, env, &kanod, "registrar", resources.TpmRegistrar, configv1.RegistrarReadyCondition, GitCertsDeploy)
	case GitCertsDeploy:
		if argocd.GitCerts != nil {
			if err := r.configureGitCerts(ctx, log, kanod.Namespace, argocd.GitCerts); err != nil {
				return ctrl.Result{}, err
			}
		}
		if err := r.setProgression(ctx, &kanod, StackWaitStable); err != nil {
			return ctrl.Result{}, err
		}
		return ctrl.Result{RequeueAfter: time.Second * RETRY_DELAY}, nil
	case StackWaitStable:
		var waitFors = []WaitFor{}
		if kanod.Spec.Ironic != nil {
			waitFors = append(waitFors, WaitFor{
				ConditionType: configv1.BaremetalReadyCondition,
				Namespaces:    []string{"baremetal-operator-system"},
			})
		}
		if kanod.Spec.Capi {
			waitFors = append(waitFors, WaitFor{
				ConditionType: configv1.ClusterApiReadyCondition,
				Namespaces: []string{
					"capm3-system",
					"capi-kubeadm-bootstrap-system",
					"capi-kubeadm-control-plane-system",
					"capi-system",
				},
			})
		}
		if kanod.Spec.ArgoCD != nil {
			waitFors = append(waitFors, WaitFor{
				ConditionType: configv1.GitopsReadyCondition,
				Namespaces: []string{
					"argocd",
				},
			})
		}
		if kanod.Spec.ArgoCD != nil && kanod.Spec.ArgoCD.ClusterDef {
			waitFors = append(waitFors, WaitFor{
				ConditionType: configv1.ClusterdefReadyCondition,
				Namespaces: []string{
					"cluster-def-system",
				},
			})
		}
		if kanod.Spec.Ingress != nil {
			waitFors = append(waitFors, WaitFor{
				ConditionType: configv1.IngressReadyCondition,
				Namespaces: []string{
					"ingress-nginx",
					"metallb-system",
				},
			})

		}
		if kanod.Spec.Ironic != nil && kanod.Spec.Ironic.Tpm != nil {
			waitFors = append(waitFors, WaitFor{
				ConditionType: configv1.RegistrarReadyCondition,
				Namespaces: []string{
					"registrar-system",
				},
			})
		}
		if r.checkDeploymentsAvailable(ctx, &kanod, waitFors) {
			meta.SetStatusCondition(
				&kanod.Status.Conditions,
				metav1.Condition{
					Type:   configv1.KanodStackReadyCondition,
					Status: metav1.ConditionTrue,
					Reason: configv1.ComponentDeployedReason,
				})
			if err := r.updateKanodStatus(&kanod); err != nil {
				return ctrl.Result{}, err
			}
			if err := r.setProgression(ctx, &kanod, ApplicationDeploy); err != nil {
				return ctrl.Result{}, err
			}
		} else {
			if err := r.updateKanodStatus(&kanod); err != nil {
				return ctrl.Result{}, err
			}
		}
		return ctrl.Result{RequeueAfter: time.Second * RETRY_DELAY}, nil
	case ApplicationDeploy:
		if kanod.Spec.ArgoCD != nil {
			if err := r.ApplyData(ctx, env, "applications", resources.Applications); err != nil {
				log.Error(err, "Cannot perform apply for apps.")
				return ctrl.Result{}, err
			}
			if r.Config.VaultUrl != "" {
				if err := r.AddApplicationPlugin(ctx, "baremetal-hosts"); err != nil {
					log.Error(err, "Cannot specialize baremetal-hosts app for Vault.")
					return ctrl.Result{}, err
				}
				if err := r.AddApplicationPlugin(ctx, "kanod-projects"); err != nil {
					log.Error(err, "Cannot specialize kanod-projects app for Vault.")
					return ctrl.Result{}, err
				}
			}
		}
		if err := r.setProgression(ctx, &kanod, Final); err != nil {
			return ctrl.Result{}, err
		}
		return ctrl.Result{RequeueAfter: time.Second * RETRY_DELAY}, nil
	case Final:
		log.Info("Reached fixpoint")
		return ctrl.Result{}, nil
	default:
		return ctrl.Result{}, fmt.Errorf("step %d not handled", step)
	}
}

// SetupWithManager sets up the controller with the Manager.
func (r *KanodReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&configv1.Kanod{}).
		Complete(r)
}

func (r *KanodReconciler) configureProject(
	ctx context.Context,
	env *Env,
	kanod *configv1.Kanod,
	kind string,
	project *configv1.ArgoProject,
) error {
	env.Setenv(fmt.Sprintf("%s_GIT", kind), project.Url)
	var secret corev1.Secret
	secretKey := client.ObjectKey{Name: project.Credentials, Namespace: kanod.Namespace}
	err := r.Client.Get(ctx, secretKey, &secret)
	if err != nil {
		r.Log.Error(err, "cannot get credentials for git project", "name", kanod.Name, "namespace", kanod.Namespace, "gitType", kind)
		return err
	}
	env.Setenv(fmt.Sprintf("BASE64_%s_USERNAME", kind), b64(string(secret.Data["username"])))
	env.Setenv(fmt.Sprintf("BASE64_%s_PASSWORD", kind), b64(string(secret.Data["password"])))
	return nil
}

func (r *KanodReconciler) configureDhcp(ctx context.Context, log logr.Logger, dhcp *configv1.Dhcp) error {
	dhcpConfig, err := json.Marshal(dhcp)
	if err != nil {
		log.Error(err, "Cannot dump DHCP config")
		return err
	}
	var dhcpConfigMap = corev1.ConfigMap{
		TypeMeta: metav1.TypeMeta{
			Kind:       "ConfigMap",
			APIVersion: "v1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      "dhcp-config",
			Namespace: "baremetal-operator-system",
		},
	}
	_, err = controllerutil.CreateOrUpdate(
		ctx, r.Client, &dhcpConfigMap,
		func() error {
			if dhcpConfigMap.Data == nil {
				dhcpConfigMap.Data = make(map[string]string)
			}
			dhcpConfigMap.Data["dhcp.yaml"] = string(dhcpConfig)
			return nil
		})
	return err
}

func (r *KanodReconciler) applyComponent(flag bool, ctx context.Context, env *Env, kanod *configv1.Kanod, name string, resource []byte, condition string, next State) (reconcile.Result, error) {
	if flag {
		r.Log.Info("Component deployment.", "component", name)
		if err := r.ApplyData(ctx, env, name, resource); err != nil {
			r.Log.Error(err, "Cannot perform apply for component.", "component", name, "name", kanod.Name, "namespace", kanod.Namespace)
			return ctrl.Result{}, err
		}
		if condition != "" {
			meta.SetStatusCondition(
				&kanod.Status.Conditions,
				metav1.Condition{
					Type:   condition,
					Status: metav1.ConditionFalse,
					Reason: configv1.WaitingForComponentReason,
				})
			if err := r.updateKanodStatus(kanod); err != nil {
				return ctrl.Result{}, err
			}
		}
	} else {
		r.Log.Info("Component not deployed.", "component", name)
	}
	if err := r.setProgression(ctx, kanod, next); err != nil {
		r.Log.Error(err, "Cannot set annotation for stack apply", name, "name", kanod.Name, "namespace", kanod.Namespace)
		return ctrl.Result{}, err
	}
	return ctrl.Result{RequeueAfter: time.Second * RETRY_DELAY}, nil

}
func (r *KanodReconciler) configureGitCerts(ctx context.Context, log logr.Logger, namespace string, gitCerts []configv1.GitCertificates) error {
	gitCertsConfigMap := corev1.ConfigMap{
		TypeMeta: metav1.TypeMeta{
			Kind:       "ConfigMap",
			APIVersion: "v1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      "argocd-tls-certs-cm",
			Namespace: "argocd",
		},
	}
	_, err := controllerutil.CreateOrUpdate(
		ctx, r.Client, &gitCertsConfigMap,
		func() error {
			if gitCertsConfigMap.Labels == nil {
				gitCertsConfigMap.Labels = make(map[string]string)
			}
			if gitCertsConfigMap.Data == nil {
				gitCertsConfigMap.Data = make(map[string]string)
			}
			gitCertsConfigMap.Labels["app.kubernetes.io/name"] = "argocd-tls-certs-cm"
			gitCertsConfigMap.Labels["app.kubernetes.io/part-of"] = "argocd"

			for _, gitCert := range gitCerts {
				value, err := r.GetValue(ctx, &gitCert.StringValue, namespace)
				if err != nil {
					return err
				}
				gitCertsConfigMap.Data[gitCert.Host] = value
			}
			return nil
		})
	return err

}

func (r *KanodReconciler) GetValue(ctx context.Context, sv *configv1.StringValue, namespace string) (string, error) {
	value := sv.Value
	if sv.ValueFrom != nil {
		var err error
		if sv.ValueFrom.ConfigMapName != "" {
			value, err = r.getCMValue(ctx, namespace, sv.ValueFrom)
		} else if sv.ValueFrom.SecretName != "" {
			value, err = r.getSecretValue(ctx, namespace, sv.ValueFrom)
		}
		if err != nil {
			return "", err
		}
	}
	return value, nil
}

func (r *KanodReconciler) getCMValue(ctx context.Context, namespace string, keyRef *configv1.KeyRef) (string, error) {
	var cm corev1.ConfigMap
	objKey := client.ObjectKey{Name: keyRef.ConfigMapName, Namespace: namespace}
	if err := r.Get(ctx, objKey, &cm); err != nil {
		return "", err
	}
	if val, ok := cm.Data[keyRef.Key]; ok {
		return val, nil
	} else {
		return "", fmt.Errorf("getCMValue; no key %s in %s/%s", keyRef.Key, keyRef.ConfigMapName, namespace)
	}

}

func (r *KanodReconciler) getSecretValue(ctx context.Context, namespace string, keyRef *configv1.KeyRef) (string, error) {
	var cm corev1.Secret
	objKey := client.ObjectKey{Name: keyRef.SecretName, Namespace: namespace}
	if err := r.Get(ctx, objKey, &cm); err != nil {
		return "", err
	}
	if val, ok := cm.Data[keyRef.Key]; ok {
		return string(val), nil
	} else {
		return "", fmt.Errorf("getSecretValue; no key %s in %s/%s", keyRef.Key, keyRef.SecretName, namespace)
	}

}

// updateReverseWordsAppStatus updates the Status of a given CR
func (r *KanodReconciler) updateKanodStatus(ka *configv1.Kanod) error {

	err := retry.RetryOnConflict(retry.DefaultRetry,
		func() error {
			kanod := &configv1.Kanod{}
			err := r.Get(context.Background(), types.NamespacedName{Name: ka.Name, Namespace: ka.Namespace}, kanod)
			if err != nil {
				return err
			}
			if !reflect.DeepEqual(ka.Status, kanod.Status) {
				r.Log.Info("Updating Status.")
				err = r.Status().Update(context.Background(), ka)
				return err
			}
			return nil
		})
	return err
}
