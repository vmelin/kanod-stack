package resources

import (
	_ "embed"
)

//go:embed cert-manager.yml
var CertManager []byte

//go:embed kn-ironic.yml
var Ironic []byte

//go:embed kn-bmoperator.yml
var BMO []byte

//go:embed kn-capi.yml
var Capi []byte

//go:embed kn-argocd.yml
var Argocd []byte

//go:embed kn-clusterdef.yml
var Clusterdef []byte

//go:embed kn-dashboard.yml
var Dashboard []byte

//go:embed kanod-ingress.yml
var Ingress []byte

//go:embed applications.yml
var Applications []byte

//go:embed tpm-registrar.yml
var TpmRegistrar []byte
