/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// KanodSpec defines the desired state of Kanod
type KanodSpec struct {
	// Name of the configmap containing the core configuration. Defaults to
	// config
	// +optional
	// +kubebuilder:validation:Optional
	// +kubebuilder:default:=config
	ConfigName string `json:"configName,omitempty"`

	// Ironic is the Ironic configuration
	Ironic *Ironic `json:"ironic,omitempty"`
	// Capi is a boolean to optionally disable CAPI
	// +optional
	// +kubebuilder:validation:Optional
	// +kubebuilder:default:=true
	Capi bool `json:"capi"`
	// CertManager is a boolean to optionally disable Cert-Manager (if provided elsewhere)
	// +optional
	// +kubebuilder:validation:Optional
	// +kubebuilder:default:=true
	CertManager bool `json:"certManager"`
	// ArgoCD is the configuration of argocd projects
	ArgoCD *ArgoCD `json:"argocd,omitempty"`
	// Ingress
	Ingress *Ingress `json:"ingress,omitempty"`
}

const (
	CertManagerReadyCondition = "CertManagerReady"
	KanodStackReadyCondition  = "KanodStackReady"
	BaremetalReadyCondition   = "BaremetalReady"
	ClusterApiReadyCondition  = "ClusterApiReady"
	GitopsReadyCondition      = "GitopsReady"
	ClusterdefReadyCondition  = "ClusterdefReady"
	RegistrarReadyCondition   = "RegistrarReady"
	IngressReadyCondition     = "IngressReady"

	WaitingForComponentReason = "WaitingForComponent"
	ComponentDeployedReason   = "ComponentDeployed"
	InternalFailureReason     = "InternalFailure"
)

// KanodStatus defines the observed state of Kanod
type KanodStatus struct {
	// conditions describe the state of the built image
	// +patchMergeKey=type
	// +patchStrategy=merge
	// +listType=map
	// +listMapKey=type
	// +optional
	Conditions []metav1.Condition `json:"conditions,omitempty" patchStrategy:"merge" patchMergeKey:"type"`
}

type Ironic struct {
	// +optional
	// +kubebuilder:validation:Optional
	// +kubebuilder:default:=true
	Bmo        bool   `json:"bmo"`
	Tpm        *Tpm   `json:"tpm,omitempty"`
	Interface  string `json:"interface"`
	IP         string `json:"ip,omitempty"`
	IpaKernel  string `json:"ipaKernel,omitempty"`
	IpaRamdisk string `json:"ipaRamdisk,omitempty"`
	DHCP       *Dhcp  `json:"dhcp,omitempty"`
}

type Ingress struct {
	IP          string       `json:"ip"`
	Name        string       `json:"name"`
	LBRange     string       `json:"loadBalancerRange"`
	Key         *StringValue `json:"key,omitempty"`
	Certificate *StringValue `json:"certificate,omitempty"`
	MetalLB     bool         `json:"metallb,omitempty"`
}

type Dhcp struct {
	Subnets []DhcpSubnet `json:"subnets"`
	Hosts   []DhcpHost   `json:"hosts,omitempty"`
	Ironic  *Endpoint    `json:"ironic,omitempty"`
}

type Tpm struct {
	AuthUrl   string       `json:"authUrl"`
	AuthCa    *StringValue `json:"authCa"`
	Registrar *Endpoint    `json:"registrar,omitempty"`
}

type Endpoint struct {
	IP   string `json:"ip"`
	Port int    `json:"port"`
}

type DhcpSubnet struct {
	Prefix  string  `json:"prefix"`
	Mask    string  `json:"mask"`
	Start   string  `json:"start"`
	End     string  `json:"end"`
	Gateway *string `json:"gateway,omitempty"`
}

type DhcpHost struct {
	Name string  `json:"name"`
	Mac  string  `json:"mac"`
	IP   *string `json:"ip,omitempty"`
}

type ArgoCD struct {
	GitCerts  []GitCertificates `json:"gitcerts,omitempty"`
	Baremetal ArgoProject       `json:"baremetal,omitempty"`
	Projects  ArgoProject       `json:"projects,omitempty"`
	// +optional
	// +kubebuilder:validation:Optional
	// +kubebuilder:default:=true
	ClusterDef bool `json:"clusterdef"`
	// +optional
	// +kubebuilder:validation:Optional
	// +kubebuilder:default:=true
	Dashboard bool `json:"dashboard"`
}

type GitCertificates struct {
	Host        string `json:"host"`
	StringValue `json:",inline"`
}

type StringValue struct {
	Value     string  `json:"value,omitempty"`
	ValueFrom *KeyRef `json:"valueFrom,omitempty"`
}

type KeyRef struct {
	ConfigMapName string `json:"configMapName,omitempty"`
	SecretName    string `json:"secretName,omitempty"`
	Key           string `json:"key,omitempty"`
}

type ArgoProject struct {
	Url         string `json:"url"`
	Credentials string `json:"credentials"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// Kanod is the Schema for the kanods API
type Kanod struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   KanodSpec   `json:"spec,omitempty"`
	Status KanodStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// KanodList contains a list of Kanod
type KanodList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Kanod `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Kanod{}, &KanodList{})
}
