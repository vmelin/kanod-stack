//go:build !ignore_autogenerated
// +build !ignore_autogenerated

/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Code generated by controller-gen. DO NOT EDIT.

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	runtime "k8s.io/apimachinery/pkg/runtime"
)

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *ArgoCD) DeepCopyInto(out *ArgoCD) {
	*out = *in
	if in.GitCerts != nil {
		in, out := &in.GitCerts, &out.GitCerts
		*out = make([]GitCertificates, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
	out.Baremetal = in.Baremetal
	out.Projects = in.Projects
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new ArgoCD.
func (in *ArgoCD) DeepCopy() *ArgoCD {
	if in == nil {
		return nil
	}
	out := new(ArgoCD)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *ArgoProject) DeepCopyInto(out *ArgoProject) {
	*out = *in
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new ArgoProject.
func (in *ArgoProject) DeepCopy() *ArgoProject {
	if in == nil {
		return nil
	}
	out := new(ArgoProject)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *Dhcp) DeepCopyInto(out *Dhcp) {
	*out = *in
	if in.Subnets != nil {
		in, out := &in.Subnets, &out.Subnets
		*out = make([]DhcpSubnet, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
	if in.Hosts != nil {
		in, out := &in.Hosts, &out.Hosts
		*out = make([]DhcpHost, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
	if in.Ironic != nil {
		in, out := &in.Ironic, &out.Ironic
		*out = new(Endpoint)
		**out = **in
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new Dhcp.
func (in *Dhcp) DeepCopy() *Dhcp {
	if in == nil {
		return nil
	}
	out := new(Dhcp)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *DhcpHost) DeepCopyInto(out *DhcpHost) {
	*out = *in
	if in.IP != nil {
		in, out := &in.IP, &out.IP
		*out = new(string)
		**out = **in
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new DhcpHost.
func (in *DhcpHost) DeepCopy() *DhcpHost {
	if in == nil {
		return nil
	}
	out := new(DhcpHost)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *DhcpSubnet) DeepCopyInto(out *DhcpSubnet) {
	*out = *in
	if in.Gateway != nil {
		in, out := &in.Gateway, &out.Gateway
		*out = new(string)
		**out = **in
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new DhcpSubnet.
func (in *DhcpSubnet) DeepCopy() *DhcpSubnet {
	if in == nil {
		return nil
	}
	out := new(DhcpSubnet)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *Endpoint) DeepCopyInto(out *Endpoint) {
	*out = *in
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new Endpoint.
func (in *Endpoint) DeepCopy() *Endpoint {
	if in == nil {
		return nil
	}
	out := new(Endpoint)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *GitCertificates) DeepCopyInto(out *GitCertificates) {
	*out = *in
	in.StringValue.DeepCopyInto(&out.StringValue)
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new GitCertificates.
func (in *GitCertificates) DeepCopy() *GitCertificates {
	if in == nil {
		return nil
	}
	out := new(GitCertificates)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *Ingress) DeepCopyInto(out *Ingress) {
	*out = *in
	if in.Key != nil {
		in, out := &in.Key, &out.Key
		*out = new(StringValue)
		(*in).DeepCopyInto(*out)
	}
	if in.Certificate != nil {
		in, out := &in.Certificate, &out.Certificate
		*out = new(StringValue)
		(*in).DeepCopyInto(*out)
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new Ingress.
func (in *Ingress) DeepCopy() *Ingress {
	if in == nil {
		return nil
	}
	out := new(Ingress)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *Ironic) DeepCopyInto(out *Ironic) {
	*out = *in
	if in.Tpm != nil {
		in, out := &in.Tpm, &out.Tpm
		*out = new(Tpm)
		(*in).DeepCopyInto(*out)
	}
	if in.DHCP != nil {
		in, out := &in.DHCP, &out.DHCP
		*out = new(Dhcp)
		(*in).DeepCopyInto(*out)
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new Ironic.
func (in *Ironic) DeepCopy() *Ironic {
	if in == nil {
		return nil
	}
	out := new(Ironic)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *Kanod) DeepCopyInto(out *Kanod) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ObjectMeta.DeepCopyInto(&out.ObjectMeta)
	in.Spec.DeepCopyInto(&out.Spec)
	in.Status.DeepCopyInto(&out.Status)
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new Kanod.
func (in *Kanod) DeepCopy() *Kanod {
	if in == nil {
		return nil
	}
	out := new(Kanod)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *Kanod) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *KanodList) DeepCopyInto(out *KanodList) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ListMeta.DeepCopyInto(&out.ListMeta)
	if in.Items != nil {
		in, out := &in.Items, &out.Items
		*out = make([]Kanod, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new KanodList.
func (in *KanodList) DeepCopy() *KanodList {
	if in == nil {
		return nil
	}
	out := new(KanodList)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *KanodList) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *KanodSpec) DeepCopyInto(out *KanodSpec) {
	*out = *in
	if in.Ironic != nil {
		in, out := &in.Ironic, &out.Ironic
		*out = new(Ironic)
		(*in).DeepCopyInto(*out)
	}
	if in.ArgoCD != nil {
		in, out := &in.ArgoCD, &out.ArgoCD
		*out = new(ArgoCD)
		(*in).DeepCopyInto(*out)
	}
	if in.Ingress != nil {
		in, out := &in.Ingress, &out.Ingress
		*out = new(Ingress)
		(*in).DeepCopyInto(*out)
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new KanodSpec.
func (in *KanodSpec) DeepCopy() *KanodSpec {
	if in == nil {
		return nil
	}
	out := new(KanodSpec)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *KanodStatus) DeepCopyInto(out *KanodStatus) {
	*out = *in
	if in.Conditions != nil {
		in, out := &in.Conditions, &out.Conditions
		*out = make([]metav1.Condition, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new KanodStatus.
func (in *KanodStatus) DeepCopy() *KanodStatus {
	if in == nil {
		return nil
	}
	out := new(KanodStatus)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *KeyRef) DeepCopyInto(out *KeyRef) {
	*out = *in
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new KeyRef.
func (in *KeyRef) DeepCopy() *KeyRef {
	if in == nil {
		return nil
	}
	out := new(KeyRef)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *StringValue) DeepCopyInto(out *StringValue) {
	*out = *in
	if in.ValueFrom != nil {
		in, out := &in.ValueFrom, &out.ValueFrom
		*out = new(KeyRef)
		**out = **in
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new StringValue.
func (in *StringValue) DeepCopy() *StringValue {
	if in == nil {
		return nil
	}
	out := new(StringValue)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *Tpm) DeepCopyInto(out *Tpm) {
	*out = *in
	if in.AuthCa != nil {
		in, out := &in.AuthCa, &out.AuthCa
		*out = new(StringValue)
		(*in).DeepCopyInto(*out)
	}
	if in.Registrar != nil {
		in, out := &in.Registrar, &out.Registrar
		*out = new(Endpoint)
		**out = **in
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new Tpm.
func (in *Tpm) DeepCopy() *Tpm {
	if in == nil {
		return nil
	}
	out := new(Tpm)
	in.DeepCopyInto(out)
	return out
}
