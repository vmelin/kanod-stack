#! /bin/bash

#  Copyright (C) 2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
set -eu

METAL3_RELEASE=v1.1.2

TAG="${NEXUS_REGISTRY}/ironic-image:${VERSION}"

docker login --username "${NEXUS_KANOD_USER}" \
    --password-stdin "${NEXUS_REGISTRY}" <<< "${NEXUS_KANOD_PASSWORD}"

docker build --build-arg http_proxy --build-arg https_proxy --build-arg no_proxy --build-arg "IRONIC_VERSION=capm3-${METAL3_RELEASE}" ironic-image --tag "${TAG}"
docker push "${TAG}"
docker image rm "${TAG}"
